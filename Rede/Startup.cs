﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rede.Startup))]
namespace Rede
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
