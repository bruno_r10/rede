﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rede.Models
{
    public class AccessPoint
    {
        public int AccessPointId { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "O Campo {0} é Obrigatório!")]
     [Index("AccessPoint_Nome", IsUnique = true)]
        public string Nome { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "O Campo {0} é Obrigatório!")]
        public string Modelo { get; set; }

        [Display(Name = "Mac Address")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(12, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 6)]
        public string Mac { get; set; }

        [Display(Name = "Mac Address Ethernet")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(12, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 6)]
        public string MacEth { get; set; }


        [Display(Name = "Assets")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(50, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 2)]
        public string Assets { get; set; }

        [Display(Name = "IP_Atual")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(100, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 10)]
        public string Ip_Atual { get; set; }

        [Display(Name = "IP_Antigo")]

        public string Ip_Antigo { get; set; }

        [Display(Name = "Canal")]
        public int Canal { get; set; }

        [Display(Name = "Observações")]
        [StringLength(150, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 10)]
        public string Obs { get; set; }
        public int EdificioId { get; set; }
        public virtual Edificio edificio { get; set; }
    }
}