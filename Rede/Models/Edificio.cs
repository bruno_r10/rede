﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rede.Models
{
    public class Edificio
    {
        [Key]
        public int EdificioId { get; set; }

        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(50, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 3)]
        [Index("Nome", IsUnique = true)]
        public string Nome { get; set; }

        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(50, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 3)]
        [Index("EdificioDescricaoIndex", IsUnique = true)]
        public string Descricao { get; set; }

        public int SiteId { get; set; }
        public virtual Site site { get; set; }
       // public virtual List<Equipamento> Equipamento { get; set; }

        public virtual List<Switchs> Switchs { get; set; }
        public virtual List<AccessPoint> AccessPoints { get; set; }


    }
}