﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rede.Models
{
    public class Switchs
    {
        [Key]
        public int Switchs_Id { get; set; }
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "O Campo {0} é Obrigatório!")]
        // [Index("UserNameIndex", IsUnique = true)]
        public string Nome { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "O Campo {0} é Obrigatório!")]
        public string Modelo { get; set; }

        [Display(Name = "Mac Address")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [RegularExpression("^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$", ErrorMessage = " Formato 00:00:00:00:00:00")]
        public string Mac { get; set; }

         [Display(Name = "Assets")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [StringLength(50, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 2)]
        public string Assets { get; set; }

        [Display(Name = "IP_Atual")]
        [Required(ErrorMessage = " O Campo {0} é Obrigatório!")]
        [RegularExpression("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.[0-9]{1,3}", ErrorMessage = " Formato ex:111.111.111.111")]
        public string Ip_Atual { get; set; }

        [RegularExpression("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.[0-9]{1,3}")]
        [Display(Name = "IP_Antigo")]

        public string Ip_Antigo { get; set; }



        [Display(Name = "Bastidor")]
        [StringLength(100, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 10)]
        public string Bastidor { get; set; }

        [Display(Name = "Observações")]
        [StringLength(150, ErrorMessage = " O Campo {0} pode ter no máximo {1} e minimo {2} caracteres ", MinimumLength = 10)]
        public string Obs { get; set; }

        public int EdificioId { get; set; }
        public virtual Edificio edificio { get; set; }
    }
}