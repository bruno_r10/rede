﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Rede.Models
{
    public class ControlContext : DbContext
    {
        public ControlContext() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

      //  public DbSet<Equipamento> Equipamento { get; set; }

        public DbSet<Edificio> Edificio { get; set; }
        public DbSet<Site> Site { get; set; }
        public DbSet<Switchs> Switchs { get; set; }
        public DbSet<AccessPoint> AccessPoints { get; set; }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}