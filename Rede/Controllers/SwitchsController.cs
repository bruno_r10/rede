﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rede.Models;

namespace Rede.Controllers
{
    public class SwitchsController : Controller
    {
        private ControlContext db = new ControlContext();

        // GET: Switchs
        public ActionResult Index()
        {
            var switchs = db.Switchs.Include(s => s.edificio);
            return View(switchs.ToList());
        }

        // GET: Switchs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Switchs switchs = db.Switchs.Find(id);
            if (switchs == null)
            {
                return HttpNotFound();
            }
            return View(switchs);
        }

        // GET: Switchs/Create
        public ActionResult Create()
        {
            ViewBag.EdificioId = new SelectList(db.Edificio, "EdificioId", "Nome");
            return View();
        }

        // POST: Switchs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Switchs_Id,Nome,Modelo,Mac,Assets,Ip_Atual,Ip_Antigo,Bastidor,Obs,EdificioId")] Switchs switchs)
        {
            if (ModelState.IsValid)
            {
                db.Switchs.Add(switchs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EdificioId = new SelectList(db.Edificio, "EdificioId", "Nome", switchs.EdificioId);
            return View(switchs);
        }

        // GET: Switchs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Switchs switchs = db.Switchs.Find(id);
            if (switchs == null)
            {
                return HttpNotFound();
            }
            ViewBag.EdificioId = new SelectList(db.Edificio, "EdificioId", "Nome", switchs.EdificioId);
            return View(switchs);
        }

        // POST: Switchs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Switchs_Id,Nome,Modelo,Mac,Assets,Ip_Atual,Ip_Antigo,Bastidor,Obs,EdificioId")] Switchs switchs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(switchs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EdificioId = new SelectList(db.Edificio, "EdificioId", "Nome", switchs.EdificioId);
            return View(switchs);
        }

        // GET: Switchs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Switchs switchs = db.Switchs.Find(id);
            if (switchs == null)
            {
                return HttpNotFound();
            }
            return View(switchs);
        }

        // POST: Switchs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Switchs switchs = db.Switchs.Find(id);
            db.Switchs.Remove(switchs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
