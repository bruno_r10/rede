﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Rede.Models
{
    public class EdificiosController : Controller
    {
        private ControlContext db = new ControlContext();

        // GET: Edificios
        public ActionResult Index()
        {
            List<Edificio> Edificios = new List<Edificio>();
            Edificios = db.Edificio.ToList();
            if (Edificios.Count == 0)
            {
               
              @ViewBag.erro = "Não tem Sites registados";

            return View("Error");
                 }
            
            var edificio = db.Edificio.Include(e => e.site);
             return View(edificio.ToList());
        }

        // GET: Edificios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edificio edificio = db.Edificio.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            return View(edificio);
        }

        // GET: Edificios/Create
        public ActionResult Create()
        {
            ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Nome");
            return View();
        }

        // POST: Edificios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EdificioId,Nome,Descricao,SiteId")] Edificio edificio)
        {

            //if (edificiodnome != null)
            //{
            //    ViewBag.erro = "Já existe um edificio com esse nome";
            //    ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Nome", edificio.SiteId);
            //    return View(edificio);
            //}
            if (ModelState.IsValid)
            {
                try
                {
                    //Edificio edificiodnome = db.Edificio.Find(edificio.Nome);
                    //if (edificiodnome == null)
                    //{
                        string site = db.Site.SingleOrDefault(e => e.SiteId == edificio.SiteId).Nome;
                        edificio.Nome = edificio.Nome + "-" + site;
                        db.Edificio.Add(edificio);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    //}
                }
                catch (Exception ex)
                {

                    ModelState.AddModelError(string.Empty, ex.Message);
                    
                    
                }


              
            }
            Response.Write("<script>alert('já existe um edificio com esse nome ou descrição');</script>");
            ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Nome", edificio.SiteId);   
 ViewBag.erro = "Já existe um edificio com esse nome";
           
  
            return View(edificio);
        }

        // GET: Edificios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edificio edificio = db.Edificio.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Nome", edificio.SiteId);
            return View(edificio);
        }

        // POST: Edificios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EdificioId,Nome,Descricao,SiteId")] Edificio edificio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(edificio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Nome", edificio.SiteId);
            return View(edificio);
        }

        // GET: Edificios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edificio edificio = db.Edificio.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            return View(edificio);
        }

        // POST: Edificios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Edificio edificio = db.Edificio.Find(id);
            db.Edificio.Remove(edificio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
